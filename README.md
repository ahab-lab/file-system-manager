
# File System Manager
File System Manager is a console application that offers the use a list of options as below:

1. Show available files
2. Search for files by extension
3. Check file size
4. Check number of lines in a file
5. Check for word existance in a text file
6. Check number of word occurance in a text file

The application prints out the result of an option alongside the date and execution time of the running function. printed messages are saved to the log.txt to be checked later when needed.

### Software bundling 
The following commands are used to bundle the project to .jar:
- javac  src/main/java/*.java  src/main/java/services/*.java  -cp  ./lib/*jar  -d ./out 
- jar cvfm "File System Manager.jar" ./src/main/resources/META-INF/MANIFEST.txt -C ./out/ .
 

![](demo.gif)