package src.main.java;

import java.util.ArrayList;
import java.util.Scanner;

import src.main.java.services.FileManagment;

/**
 * this class shows a list of options to the user,
 * and handles the user input
 */
public class Application {

    private Scanner scanner;

    public Application() {
        scanner = new Scanner(System.in);
    }

    /**
     * handles the whole applicaiton
     */
    public void run() {
        FileManagment handler = new FileManagment();
        String ans;

        do {
            menu();
            ans = scanner.nextLine();

            switch (ans) {
                case "1":
                    handler.getFilesList();
                    break;
                case "2":
                    ArrayList<String> list = handler.getAvailableExtensions();
                    int temp;

                    System.out.println("--------------------");
                    System.out.println("Choose an extensions");
                    System.out.println("--------------------");
                    temp = inputReader(list.size());

                    handler.searchByExtension(list.get(temp - 1));
                    break;
                case "3":
                    list = handler.getFilesList();
                    temp = inputReader(list.size());
                    handler.printFileSize(list.get(temp - 1));
                    break;
                case "4":
                    list = handler.getFilesList();
                    temp = inputReader(list.size());
                    handler.printNumberOfLines(list.get(temp - 1));
                    break;
                case "5":
                case "6":
                    System.out.print("Enter a word to check: ");
                    ans = scanner.nextLine();
                    handler.wordIsAvailable(ans);
                    break;
                case "7":
                    System.out.println("Thanks for using File system manager applicaiton.");
                    break;
                default:
                    System.out.println("Please choose one of the available options!");
                    break;
            }

        } while (!ans.equals("7"));
    }
    
    /**
     * prints out the menu to the screen
     */
    private void menu() {
        System.out.println("----------------------------------");
        System.out.println("------- File sytem manager -------");
        System.out.println("----------------------------------");
        System.out.println("1 - Show available files.");
        System.out.println("2 - Show files filtered by extension.");
        System.out.println("3 - Check file size.");
        System.out.println("4 - Check number of lines in a file.");
        System.out.println("5 - Search for a specific word existance in the text file.");
        System.out.println("6 - How many times does a word appear in the text file.");
        System.out.println("7 - Exit");
    }

    /**
     * reads user input and check if it is valid option 
     * @param size number of the available options
     * @return user input
     */
    private int inputReader(int size) {
        String ans = "";
        int option;

        while (true) {
            try {
                ans = scanner.nextLine();
                option = Integer.parseInt(ans);

                if (option <= size && option > 0) {
                    return option;
                } else {
                    System.out.println("Wrong input! Select one of the available options.");
                }

            } catch (NumberFormatException e) {
                System.out.println("-------------------------------");
                System.out.println("Wrong input! Please use digits.");
                System.out.println("-------------------------------");
            }
        }
    }
}