package src.main.java.services;

import java.time.format.DateTimeFormatter;
import java.time.LocalDateTime;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.time.Duration;
/**
 * This class prints out a message alongside the date of today and the executing time of the function in ms
 * then it logs that message to the txt file
 */
public class Logger {

    /**
     * prints the message, date and execute time
     * then adds a copy to logs
     * @param message 
     * @param time execute time
     */
    public void printOut(String message, Duration time){
        DateTimeFormatter date = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm");  
        String txt = date.format(LocalDateTime.now()) + " " + message + " the function took: " + time.toMillis() + "ms to execute.";
        System.out.println(txt);
        System.out.println("---------------------------------------------");
        addtoLog(txt);
    }

    /**
     * saves the message to logs
     * @param log
     */
    private void addtoLog(String log){
        FileWriter fileWriter;
        try {
            // create file logs.txt if it does not exist
            File file = new File("src/main/log/logs.txt");
            file.createNewFile();

            //write to logs.txt
            fileWriter = new FileWriter("src/main/log/logs.txt", true);
            fileWriter.write(log + "\n");
            fileWriter.close();
        } catch (IOException e) {
            System.out.println(e.getMessage());
            
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        } 
    }
}