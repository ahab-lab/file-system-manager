package src.main.java.services;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileFilter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.time.Duration;
import java.time.Instant;
import java.util.ArrayList;

import org.apache.commons.io.filefilter.WildcardFileFilter;

/**
 * this class handles user requests
 */
public class FileManagment {

    private String path = "./src/main/resources/";
    
    /**
     * prints a list of the available files
     * @return list of the files' name
     */
    public ArrayList<String> getFilesList() {
        ArrayList<String> list = new ArrayList<>();

        try {
            File folder = new File(path);
            File[] files = folder.listFiles();
            System.out.println("-----------------");
            System.out.println("Available files: ");
            System.out.println("-----------------");
            
            for (int i = 0; i < files.length; i++) {
                System.out.println((i + 1) + "_ " + files[i].getName());
                list.add(files[i].getName());
            }
            return list;
        } catch (NullPointerException e) {
            System.out.println("Could not find the file with the path: " + path);
        } catch (SecurityException e) {
            System.out.println("Access denied!");
        } catch (Exception e) {
            System.out.println("Could not run the function. Check logs!");
        }
        return list;
    }
    /**
     * gets the available extensions and print them out for the user
     * @return a list of the available extensions
     */
    public ArrayList<String> getAvailableExtensions() {
        ArrayList<String> extensions = new ArrayList<>();
        String ext = "";

        try {
            File folder = new File(path);
            File[] files = folder.listFiles();

            for (int i = 0; i < files.length; i++) {
                ext = files[i].getName().substring(files[i].getName().lastIndexOf(".") + 1); // ext hold the file extension name

                if (!extensions.contains(ext)) {
                    extensions.add(ext);
                }
            }

            System.out.println("----------------------");
            System.out.println("Available extensions: ");
            System.out.println("----------------------");

            for (int j = 0; j < extensions.size(); j++) {
                System.out.println((j + 1) + "- " + extensions.get(j));
            }

            return extensions;
        } catch (NullPointerException e) {
            System.out.println("Could not find the file with the path: " + path);
        } catch (SecurityException e) {
            System.out.println("Access denied!");
        } catch (Exception e) {
            System.out.println(e.getMessage());
            System.out.println("Could not run the function. Check logs!");
        }
        return extensions;
    }

    /**
     * looks for files with specific extension
     * 
     * @param extension 
     */
    public void searchByExtension(String extension) {

        try {
            File folder = new File(path);
            FileFilter fileFilter = new WildcardFileFilter("*." + extension);// to filter by extension
            File[] files = folder.listFiles(fileFilter);

            if (files.length == 0) {
                System.out.println("There are no files with the extension " + "\"" + extension + "\".");
                return;
            }

            for (int i = 0; i < files.length; i++) {
                System.out.println((i + 1) + "_ " + files[i].getName());
            }
        } catch (NullPointerException e) {
            System.out.println("Could not find the file with the path: " + path);
        } catch (IllegalArgumentException e) {
            System.out.println("Could not run the function. Check logs!");
        } catch (SecurityException e) {
            System.out.println("Access denied!");
        } catch (Exception e) {
            System.out.println("Could not run the function. Check logs!");
        }
    }

    /**
     * prints out the selected file size
     * 
     * @param fileName
     */
    public void printFileSize(String fileName) {

        try {
            Instant start = Instant.now();
            long fileSize = new File(path + fileName).length(); 
            Instant end = Instant.now();

            new Logger().printOut("The file size is " + fileSize / 1024 + " kilobytes.", Duration.between(start, end));
        } catch (NullPointerException e) {
            System.out.println(" Failed to load the file with the path: " + path + fileName);
        } catch (Exception e) {
            System.out.println("Failed to run the function. Check logs for more information.");
        }
    }

    /**
     * print out the number of line in a file.
     */
    public void printNumberOfLines(String fileName) {
        Instant start = Instant.now();

        try (BufferedReader reader = new BufferedReader(new FileReader(path + fileName))) {
            int lines = 0;

            while (reader.readLine() != null) {
                lines++;
            }
            Instant end = Instant.now();

            new Logger().printOut("There are " + lines + " lines.", Duration.between(start, end));
        } catch (FileNotFoundException e) {
            System.out.println(" Failed to load the file with the path: " + path);
        } catch (IOException e) {
            System.out.println("Could not run the function. Check logs for more information.");
        }
    }

    /**
     * prints how many times a word has been found in a text
     * @param fileName
     */
    public void wordIsAvailable(String target) {
        Instant start = Instant.now();

        try (BufferedReader reader = new BufferedReader(new FileReader(path + "Dracula.txt"))) {
            String text = "";
            int count = 0;
            StringBuilder buffer = new StringBuilder(); // the function took 4225ms when using string and 18ms when
                                                        // using bufferedBuilder

            while ((text = reader.readLine()) != null) { // save text from the file to buffer
                buffer.append(" " + text);
            }
            String[] list = buffer.toString().split("\\W");// to remove non alphabitical characters

            for (String word : list) {
                if (word.equalsIgnoreCase(target)) {
                    count++;
                }
            }
            Instant end = Instant.now();

            new Logger().printOut("The term \"" + target + "\" was found " + count + " times.",
                    Duration.between(start, end));
 
        } catch (FileNotFoundException e) {
            System.out.println(" Failed to load the file with the path: " + path + "Dracula.txt");
        } catch (IOException e) {
            System.out.println("Could not run the function.");
        }
    }
}